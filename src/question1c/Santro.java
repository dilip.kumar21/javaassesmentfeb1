package question1c;

public class Santro extends Car implements BasicCar {
	public void remoteStart() {
		System.out.println("Started Successfully");

	}

	@Override
	public void gearChange() {   //override from basiccar class
		System.out.println("Gear Changed ");

	}

	@Override
	public void music() {
		System.out.println("Music is on");

	}

	public static void main(String[] args) {
		Santro s = new Santro();
		s.music();
		s.gearChange();
		s.remoteStart();
		s.drive();
		s.stop();
	}

}
