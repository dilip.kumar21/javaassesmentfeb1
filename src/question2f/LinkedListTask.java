package question2f;

import java.util.LinkedList;
import java.util.List;

public class LinkedListTask {
	public static void main(String[] args) {
		
		List<String> li = new LinkedList<String>();
        li.add("Pepsi");
        li.add("Coke");
        li.add("Slice");
        li.add("Limca");
        li.add("Mirinda");
        System.out.println("size of list " + li.size());
        System.out.println("content present in the list : " + li);
        
        li.remove(1);
        System.out.println("after removing :" + li);


        if (li.contains("Coke")) {
            System.out.println("coke is present");
        } else {
            System.out.println("coke is not present");
        }
    }
}


