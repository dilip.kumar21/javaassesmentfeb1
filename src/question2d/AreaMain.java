package question2d;

public class AreaMain {

	public static void main(String[] args) {
		AreaBase ab = new AreaBase();
		ab.squareArea(6);
		ab.rectangleArea(10, 15);
		ab.circleArea(3.5);

	}

}
